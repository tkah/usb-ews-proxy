# USB EWS proxy

Enables browser access to Embedded Web Server (EWS) in HP p1102w USB
printer.

## Overview

The EWS provides printer information and various settings but can not be
accessed with a web browser directly over USB.

`usb-ews-proxy` listens on a network port (on localhost by default)
and passes the received requests to the EWS endpoint in the printer.
Responses from the USB endpoint are sent back to the requester.

The USB EWS proxy was inspired by
[EWS Gateway](https://www.ktpanda.org/software/ewsgateway/).

## Requirements

Python 3.6 or later is needed for running and
[pyusb](https://pyusb.github.io/pyusb/) 1.1.0 or later for USB printer
access.

pyusb can be installed in a Python virtualenv if OS package for is not
available.

```
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $  pip3 install -r requirements.txt
Collecting pyusb>=1.1.0
  Using cached pyusb-1.1.0-py3-none-any.whl
Installing collected packages: pyusb
Successfully installed pyusb-1.1.0
```

## Usage

First connect the printer and turn it on. The USB device should become
available in a few seconds after the printer has initialized.

Example usage in Bash. Sudo might be necessary to be able to access the
USB device.

```
$ sudo ./usb-ews-proxy
Looking for USB device with vendor id 0x3f0 and product id 0x32a
Device found
Listen on 127.0.0.1:8080, press Ctrl-c to quit
```

Open http://127.0.0.1:8080/ in a browser to see the printer status page.

Pressing `Ctrl-C` will exit the proxy.

While listening on non-localhost address is possible
(see `./usb-ews-proxy --help`) it is better to keep the EWS away from
public internet.

## License

This project is licensed under the GNU GPLv3 license - See the
[LICENSE](LICENSE) file for details.
